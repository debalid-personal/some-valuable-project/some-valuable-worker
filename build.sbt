lazy val commonSettings = Seq(
  organization := "com.staysanta",
  scalaVersion := "2.12.3",
  version := "0.1.0-SNAPSHOT"
)

lazy val api = (project in file("api"))
  .settings(commonSettings)
  .settings(
    name := "some-valuable-worker-api",
    version := "0.1.1-SNAPSHOT",
    libraryDependencies ++= Seq(
      "com.fasterxml.jackson.core" % "jackson-annotations" % "2.9.0"
    ),
    crossPaths := false,
    publishTo := Some(Resolver.file("file", new File(Path.userHome.absolutePath + "/.m2/repository")))
  )

import sbtdocker.DockerPlugin.autoImport.imageNames
val akkaVersion = "2.4.19"
lazy val core = (project in file("core"))
  .dependsOn(api)
  .enablePlugins(DockerPlugin)
  .settings(commonSettings)
  .settings(
    name := "some-valuable-worker",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-actor" % akkaVersion,
      "com.lightbend.akka" %% "akka-stream-alpakka-amqp" % "0.11",
      "com.typesafe" % "config" % "1.2.0",
      "com.fasterxml.jackson.core" % "jackson-databind" % "2.9.0",
      "com.fasterxml.jackson.module" %% "jackson-module-scala" % "2.9.0",
      "com.fasterxml.jackson.datatype" % "jackson-datatype-jsr310" % "2.9.0",
      "com.google.guava" % "guava" % "23.0",
      "org.scalatest" %% "scalatest" % "3.0.1" % "test",
      "com.typesafe.akka" %% "akka-testkit" % akkaVersion
    ),
    mainClass in assembly := Some("com.staysanta.somevaluableworker.WorkerApp"),
    dockerfile in docker := {
      // The assembly task generates a fat JAR file
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      val waitForItScript = assembly.value.toPath.getParent.resolve("./classes/wait-for")
      val waitForItScriptTargetPath = "/wait-for"

      new Dockerfile {
        from("openjdk:alpine")
        add(artifact, artifactTargetPath)
        add(waitForItScript.toFile, waitForItScriptTargetPath)
	      run("chmod", "+x", "/wait-for")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    },
    imageNames in docker := Seq(
      // Sets the latest tag
      ImageName(s"${organization.value}/${name.value}:latest"),

      // Sets a name with a tag that contains the project version
      ImageName(
        namespace = Some(organization.value),
        repository = name.value,
        tag = Some("v" + version.value)
      )
    )
  )

lazy val root = (project in file("."))
  .aggregate(api, core)

org.jetbrains.sbt.StructureKeys.sbtStructureOptions in Global := "download resolveClassifiers"

