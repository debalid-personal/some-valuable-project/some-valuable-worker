package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.time.ZonedDateTime;
import java.util.UUID;

@JsonTypeName("taskStarted")
public class TaskStarted extends TaskNotification {
    private final ZonedDateTime startedAt;

    @JsonCreator
    public TaskStarted(@JsonProperty("id") UUID id, @JsonProperty("startedAt") ZonedDateTime startedAt) {
        super(id);
        this.startedAt = startedAt;
    }

    public ZonedDateTime getStartedAt() {
        return startedAt;
    }

    @Override
    public void acceptVisitor(TaskNotificationVisitor visitor) {
        visitor.visitTaskStarted(this);
    }

    @Override
    public String toString() {
        return "TaskStarted{" +
                "startedAt=" + startedAt +
                ", id=" + id +
                '}';
    }
}
