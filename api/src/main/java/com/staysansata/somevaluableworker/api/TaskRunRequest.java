package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.*;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonTypeName("taskRunRequest")
public class TaskRunRequest extends TaskRequest {
    public enum Algorithm {
        MD5("md5"), SHA_1("sha-1"), SHA_256("sha-256");

        private final String value;

        Algorithm(String value) {
            this.value = value;
        }

        @JsonValue
        public String getValue() {
            return value;
        }
    }

    private final String src;

    private final Algorithm algorithm;

    @JsonCreator
    public TaskRunRequest(@JsonProperty("id") UUID id, @JsonProperty("src") String src, @JsonProperty("algo") Algorithm algorithm) {
        super(id);
        this.src = src;
        this.algorithm = algorithm;
    }

    @JsonProperty("id")
    public UUID getId() {
        return id;
    }

    @JsonProperty("src")
    public String getSrc() {
        return src;
    }

    @JsonProperty("algo")
    public Algorithm getAlgorithm() {
        return algorithm;
    }

    @Override
    public String toString() {
        return "TaskRunRequest{" +
                "src='" + src + '\'' +
                ", algorithm=" + algorithm +
                ", id=" + id +
                '}';
    }
}
