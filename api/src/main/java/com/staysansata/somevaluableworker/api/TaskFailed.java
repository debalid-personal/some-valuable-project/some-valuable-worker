package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.time.ZonedDateTime;
import java.util.UUID;

@JsonTypeName("taskFailed")
public class TaskFailed extends TaskNotification {
    private final ZonedDateTime finishedAt;

    private final String error;

    @JsonCreator
    public TaskFailed(@JsonProperty("id") UUID id, @JsonProperty("finishedAt") ZonedDateTime finishedAt, @JsonProperty("error") String error) {
        super(id);
        this.finishedAt = finishedAt;
        this.error = error;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public String getError() {
        return error;
    }

    @Override
    public void acceptVisitor(TaskNotificationVisitor visitor) {
        visitor.visitTaskFailed(this);
    }

    @Override
    public String toString() {
        return "TaskFailed{" +
                "finishedAt=" + finishedAt +
                ", error='" + error + '\'' +
                ", id=" + id +
                '}';
    }
}
