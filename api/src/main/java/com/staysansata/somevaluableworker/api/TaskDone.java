package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.time.ZonedDateTime;
import java.util.UUID;

@JsonTypeName("taskDone")
public class TaskDone extends TaskNotification {
    private final ZonedDateTime finishedAt;

    private final String digest;

    @JsonCreator
    public TaskDone(@JsonProperty("id") UUID id, @JsonProperty("finishedAt") ZonedDateTime finishedAt, @JsonProperty("digest") String digest) {
        super(id);
        this.finishedAt = finishedAt;
        this.digest = digest;
    }

    public ZonedDateTime getFinishedAt() {
        return finishedAt;
    }

    public String getDigest() {
        return digest;
    }

    @Override
    public void acceptVisitor(TaskNotificationVisitor visitor) {
        visitor.visitTaskDone(this);
    }

    @Override
    public String toString() {
        return "TaskDone{" +
                "finishedAt=" + finishedAt +
                ", digest='" + digest + '\'' +
                ", id=" + id +
                '}';
    }
}
