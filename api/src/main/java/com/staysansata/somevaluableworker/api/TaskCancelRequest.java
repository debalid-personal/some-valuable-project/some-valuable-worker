package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonTypeName("taskCancelRequest")
public class TaskCancelRequest extends TaskRequest {

    @JsonCreator
    public TaskCancelRequest(@JsonProperty("id") UUID id) {
        super(id);
    }

    @Override
    public String toString() {
        return "TaskCancelRequest{" +
                "id=" + id +
                '}';
    }
}
