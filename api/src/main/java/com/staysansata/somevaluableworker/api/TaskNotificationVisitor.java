package com.staysansata.somevaluableworker.api;

public interface TaskNotificationVisitor {
    void visitTaskStarted(TaskStarted notification);

    void visitTaskCanceled(TaskCanceled notification);

    void visitTaskDone(TaskDone notification);

    void visitTaskFailed(TaskFailed notification);
}
