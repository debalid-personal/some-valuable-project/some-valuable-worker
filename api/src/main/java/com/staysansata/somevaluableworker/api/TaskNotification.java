package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.util.UUID;

@JsonSubTypes(value = {
        @JsonSubTypes.Type(TaskStarted.class),
        @JsonSubTypes.Type(TaskCanceled.class),
        @JsonSubTypes.Type(TaskDone.class),
        @JsonSubTypes.Type(TaskFailed.class)
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public abstract class TaskNotification {
    protected final UUID id;

    protected TaskNotification(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public abstract void acceptVisitor(TaskNotificationVisitor visitor);
}
