package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.*;

import java.util.UUID;

@JsonSubTypes(value = {
        @JsonSubTypes.Type(TaskRunRequest.class),
        @JsonSubTypes.Type(TaskCancelRequest.class)
})
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
public abstract class TaskRequest {
    protected final UUID id;

    protected TaskRequest(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }
}
