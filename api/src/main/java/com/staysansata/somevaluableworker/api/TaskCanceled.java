package com.staysansata.somevaluableworker.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import java.time.ZonedDateTime;
import java.util.UUID;

@JsonTypeName("taskCanceled")
public class TaskCanceled extends TaskNotification {
    private final ZonedDateTime canceledAt;

    @JsonCreator
    public TaskCanceled(@JsonProperty("id") UUID id, @JsonProperty("canceledAt") ZonedDateTime canceledAt) {
        super(id);
        this.canceledAt = canceledAt;
    }

    public ZonedDateTime getCanceledAt() {
        return canceledAt;
    }

    @Override
    public void acceptVisitor(TaskNotificationVisitor visitor) {
        visitor.visitTaskCanceled(this);
    }

    @Override
    public String toString() {
        return "TaskCanceled{" +
                "canceledAt=" + canceledAt +
                ", id=" + id +
                '}';
    }
}
