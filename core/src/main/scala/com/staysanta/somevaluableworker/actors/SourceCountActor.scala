package com.staysanta.somevaluableworker.actors

import java.time.Instant
import java.util.concurrent.TimeUnit

import akka.actor.{Actor, ActorRef, Props}
import akka.event.Logging
import com.staysansata.somevaluableworker.api.TaskRunRequest
import com.staysanta.somevaluableworker.actors

import scala.collection.mutable
import scala.concurrent.duration._

/**
  * Represents a shared resource of URL links.
  * If resource is no more available (max count at time period is reached), requested actor reference will be responsed with
  * [[actors.SourceCountActor.RejectSource]] or [[actors.SourceCountActor.ApproveSource]]
  * otherwise.
  */
class SourceCountActor extends Actor {

  import SourceCountActor._

  private lazy val log = Logging(this)
  private lazy val actorAccessQueuesByUrl: mutable.Map[String, mutable.Queue[(ActorRef, Instant)]] = mutable.WeakHashMap()

  override def receive: Receive = {
    case RequestSource(task) =>
      log.debug("Current URL counts state: {}", actorAccessQueuesByUrl)
      actorAccessQueuesByUrl.get(task.getSrc) match {
        case Some(queue) =>
          queue.dequeueAll({
            case (actor, accessTime) => accessTime.isBefore(Instant.now.minusSeconds(TimeOut.toSeconds))
          }).foreach(log.debug("URL expired: {}", _))

          sender() ! (if (queue.size < MaxConnectionsToUrl) {
            queue.enqueue(sender() -> Instant.now())
            log.debug("Source {} approved for {}", task.getSrc, sender())
            ApproveSource(task)
          } else {
            val secondsLeft = (Duration(queue.front._2.toEpochMilli - Instant.now().toEpochMilli, TimeUnit.MILLISECONDS) + TimeOut).toSeconds
            log.debug("Source {} reached its limit, {} seconds left...", task.getSrc, secondsLeft)
            RejectSource(task)
          })

        case None =>
          actorAccessQueuesByUrl.put(task.getSrc, mutable.Queue(sender() -> Instant.now()))
          sender() ! ApproveSource(task)
      }
  }
}

object SourceCountActor {
  //TODO: configuration
  val MaxConnectionsToUrl: Int = 3
  val TimeOut: FiniteDuration = 1.minutes

  def props: Props = Props[SourceCountActor](new SourceCountActor)

  case class RequestSource(task: TaskRunRequest)

  case class ApproveSource(task: TaskRunRequest)

  case class RejectSource(task: TaskRunRequest)

}
