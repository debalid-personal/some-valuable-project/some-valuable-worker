package com.staysanta.somevaluableworker.actors

import java.io._
import java.net.URL
import java.nio.file.{Files, Paths}
import java.security.{DigestInputStream, MessageDigest}
import java.time.ZonedDateTime
import java.util.UUID

import akka.actor.{Actor, ActorRef, DiagnosticActorLogging, Props}
import akka.event.Logging.MDC
import com.google.common.io.BaseEncoding
import com.staysansata.somevaluableworker.api.{TaskRunRequest, _}

/**
  * Represents a job of downloading a remote file and calculating a digest according to [[TaskRunRequest]]
  *
  * @param notificationsRef ActorRef to where job status notifications are sent
  */
class WorkerActor(notificationsRef: ActorRef) extends Actor with DiagnosticActorLogging {

  import WorkerActor._

  override def mdc(message: Any): MDC = message match {
    case task: TaskRequest =>
      Map(TaskId -> task.getId)
    case _ =>
      Map.empty
  }

  override def receive: Receive = {
    case task: TaskRunRequest =>
      log.debug("Received a new task request: {}", task)
      val url = new URL(task.getSrc)
      val started = new TaskStarted(task.getId, ZonedDateTime.now())
      notificationsRef ! started
      context.parent ! started

      val messageDigest = MessageDigest.getInstance(task.getAlgorithm.getValue)

      val tmpFilePath = Paths.get(System.getProperty(SystemTempDirProperty)).resolve(s"digest/${task.getId.toString}")
      if (Files.notExists(tmpFilePath)) {
        Files.createFile(tmpFilePath)
      }

      val digestInputStream = new DigestInputStream(new BufferedInputStream(url.openStream()), messageDigest)
      val fileOutputStream = new BufferedOutputStream(new FileOutputStream(tmpFilePath.toFile))

      self ! ShouldContinue(task.getId)
      context.become(processDownloading(task, url, digestInputStream, fileOutputStream, new Array[Byte](BufferSize)))
  }

  /**
    * Handles downloading of a given file to the system temp directory, files is named according to task request id (assuming it's unique)
    * Digest is calculated at the same time.
    * Parent supervisor is asked to continue regularly.
    */
  def processDownloading(task: TaskRequest,
                         url: URL,
                         inputStream: DigestInputStream,
                         outputStream: OutputStream,
                         buffer: Array[Byte]): Receive = {
    case ShouldContinue(uuid) =>
      try {
        val read = inputStream.read(buffer)
        if (read > 0) {
          outputStream.write(buffer, 0, read)
          context.parent ! AskShouldContinue(task.getId)
        } else {
          //Download is finished
          val digest = digestToString(inputStream.getMessageDigest.digest())
          log.debug("Finished, result is: {}", digest)

          inputStream.close()
          outputStream.close()

          val done = new TaskDone(task.getId, ZonedDateTime.now(), digest)
          notificationsRef ! done
          context.parent ! done

          context.become(receive)
        }
      } catch {
        case any: Throwable =>
          inputStream.close()
          outputStream.close()
          throw any
      }

    case ShouldCancel(uuid) =>
      log.debug("Cancel task {}", uuid)
      inputStream.close()
      outputStream.close()
      context.become(receive)

      val canceled = new TaskCanceled(uuid, ZonedDateTime.now())
      notificationsRef ! canceled
      context.parent ! canceled

    case task: TaskRunRequest =>
      //Nothing restrict a router to route this message to busy worker. That's a simple WA
      context.parent ! IsBusy(task, sender())

  }

  private def digestToString(digest: Array[Byte]): String = BaseEncoding.base16().lowerCase().encode(digest)

  override def preRestart(reason: Throwable, message: Option[Any]): Unit = {
    def notifyFailure(task: TaskRequest): Unit = {
      val failed = createTaskFailedMessage(task.getId, reason)
      notificationsRef ! failed
      context.parent ! failed
    }

    message match {
      case Some(task: TaskRequest) =>
        notifyFailure(task)

      case other =>
        log.error("Unhandled message {} or exception, output stream will not be notified! {}", message, reason)
    }
    super.preRestart(reason, message)
  }

  private def createTaskFailedMessage(taskId: UUID, error: Throwable) = {
    log.error("Unexpected error: {}", error)
    val sw = new StringWriter
    error.printStackTrace(new PrintWriter(sw))
    new TaskFailed(taskId, ZonedDateTime.now(), sw.toString)
  }
}

object WorkerActor {
  //TODO: configuration
  val BufferSize = 1024

  val TaskId = "taskId"
  val SystemTempDirProperty = "java.io.tmpdir"

  def props(notificationsRef: ActorRef) = Props(new WorkerActor(notificationsRef))

  case class AskShouldContinue(taskId: UUID)

  case class ShouldContinue(taskId: UUID)

  case class ShouldCancel(taskId: UUID)

  case class IsBusy(task: TaskRunRequest, requester: ActorRef)

}