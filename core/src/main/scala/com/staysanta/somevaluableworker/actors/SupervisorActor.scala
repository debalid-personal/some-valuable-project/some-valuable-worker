package com.staysanta.somevaluableworker.actors

import java.nio.file.{Files, Paths}
import java.time.ZonedDateTime
import java.util.UUID

import akka.actor.{Actor, ActorRef, Props, Terminated}
import akka.event.Logging
import akka.routing._
import akka.util.Timeout
import com.staysansata.somevaluableworker.api._
import com.staysanta.somevaluableworker.actors.SourceCountActor.{ApproveSource, RejectSource, RequestSource}
import com.staysanta.somevaluableworker.actors.WorkerActor._

import scala.collection.mutable

/**
  * Supervises worker actors, manages task assigning and balances the loading (5 workers by default)
  * Handled cancellation of worker actors using "passive" strategy, see [[handleShouldWorkerContinue()]]
  * Also handles a back pressure to input stream
  *
  * @param notificationsRef  ActorRef to where job status notifications are sent
  */
class SupervisorActor(notificationsRef: ActorRef, requeueRef: ActorRef) extends Actor {
  import SupervisorActor._

  private lazy val log = Logging(this)
  private lazy val inMemoryTaskQueue: mutable.Queue[(TaskRunRequest, ActorRef)] = mutable.Queue()
  private lazy val workerByTaskId: mutable.Map[UUID, ActorRef] = mutable.HashMap()
  private lazy val canceledTasks: mutable.Set[UUID] = mutable.HashSet()
  private lazy val sourceCountActorRef = context.actorOf(SourceCountActor.props, "urlRefCount")
  private var workerCount = 0

  private var router: Router = {
    val workers = for (x <- 1 to MaxWorkersCount) yield createRoutee(x)
    Router(RoundRobinRoutingLogic(), workers)
  }

  private def createRoutee(num: Long): ActorRefRoutee = {
    val ref = context.actorOf(WorkerActor.props(notificationsRef), s"worker-$num")
    context.watch(ref)
    workerCount += 1
    ActorRefRoutee(ref)
  }

  override def receive: Receive = handleTaskRequest orElse handleTaskNotification orElse handleShouldWorkerContinue orElse handleBusyWorker orElse  {
    case Terminated(ref) =>
      router.removeRoutee(ref)
      router = router.addRoutee(createRoutee(workerCount + 1))
  }

  /**
    * 1) One approach is to send [[Acknowledgment]] to input stream (= job requester) only when we really have a free worker,
    * in this case [[inMemoryTaskQueue]] won't be overloaded (= [[Acknowledgment]] is sent as soon as when worker receives a job), <b>but</b>
    * 2) [[TaskCancelRequest]] is sourced from the same queue as [[TaskRunRequest]], so if we have a back pressure then
    * it's more likely that cancel requests won't reach underlying worker at right time. If this is not desirable, we may send [[Acknowledgment]]
    * as soon as this actor receives a request (it's implemented this way now)
    *
    * TODO: think about the best approach. Maybe separated queues for [[TaskRunRequest]] and [[TaskCancelRequest]] (but how to guarantee ordering???) ???
    *
    * Sender is assumed to be an input AMQP stream.
    */
  def handleTaskRequest(): Receive = {
    case request: TaskRunRequest =>
      inMemoryTaskQueue.enqueue(request -> sender())
      assignNextJob()
      sender() ! Acknowledgment()

    case cancelRequest: TaskCancelRequest =>
      if (workerByTaskId.contains(cancelRequest.getId) || inMemoryTaskQueue.exists(_._1.getId == cancelRequest.getId)) {
        canceledTasks.add(cancelRequest.getId)
      } else {
        log.debug("Task id {} was already finished or never started, impossible to cancel...", cancelRequest.getId)
      }
      sender() ! Acknowledgment()
  }

  private def assignNextJob(): Unit = {
    //TODO: avoid ask pattern - m.b. context switch with Stash?
    import akka.pattern.ask
    import context.dispatcher
    implicit lazy val timeout = Timeout.durationToTimeout(SourceCountActor.TimeOut)

    if (workerByTaskId.size < MaxWorkersCount) {
      //No need to schedule cancelled job
      inMemoryTaskQueue.dequeueAll(x => canceledTasks.contains(x._1.getId)).foreach(canceled => {
        canceledTasks.remove(canceled._1.getId)
        notificationsRef ! new TaskCanceled(canceled._1.getId, ZonedDateTime.now())
      })

      if (inMemoryTaskQueue.nonEmpty) {
        val (task, jobRequester) = inMemoryTaskQueue.dequeue()
        (sourceCountActorRef ? RequestSource(task)).map({
          case ApproveSource(_) =>
            log.debug("Source {} is free, continue", task.getSrc)
            router.route(task, jobRequester)

          case RejectSource(_) =>
            log.debug("Source was rejected, task [{}] will be retried later", task)
            //inMemoryTaskQueue.enqueue(task -> jobRequester)
            requeueRef ! task
        })
        //TODO: see a comment above #handleTaskRequest
        //jobRequester ! Acknowledgment()
      }
    } else {
      log.debug("All {} workers are busy, job will start later...")
    }
  }

  private def assignNextJob(finished: TaskNotification): Unit = {
    workerByTaskId.remove(finished.getId)
    canceledTasks.remove(finished.getId)
    assignNextJob()
  }

  /**
    * Normal task processing. Sender is a child worker
    */
  def handleTaskNotification(): Receive = {
    case started: TaskStarted =>
      val workerRef = sender()
      workerByTaskId.put(started.getId, workerRef)
      log.debug("Task {} was assigned to {}", started.getId, workerRef)

    case done: TaskDone =>
      log.debug("Task {} was done by {}", done.getId, sender())
      assignNextJob(done)

    case failed: TaskFailed =>
      log.debug("Task {} was failed by {}", failed.getId, sender())
      assignNextJob(failed)

    case canceled: TaskCanceled =>
      log.debug("Task {} was canceled by {}", canceled.getId, sender())
      assignNextJob(canceled)
  }

  /**
    * "Passive" strategy. Input stream places a request to cancel the task in [[handleTaskRequest]], cancelled tasks are stored in [[canceledTasks]]
    * Workers ask supervisor should they continue their task using [[AskShouldContinue]] in timely manner, see [[WorkerActor.processDownloading()]] for details
    * Sender is a child worker.
    */
  def handleShouldWorkerContinue(): Receive = {
    case AskShouldContinue(uuid: UUID) =>
      sender() ! (if (canceledTasks.contains(uuid)) {
        canceledTasks.remove(uuid)
        ShouldCancel(uuid)
      } else ShouldContinue(uuid))
  }

  /**
    * In current implementation a router may send a message to a busy worker, in this case we just requeue the job.
    * TODO: find out a better solution - m.b. Stash on worker?
    */
  def handleBusyWorker(): Receive = {
    case IsBusy(task, requester) =>
      //inMemoryTaskQueue.enqueue(task -> requester)
      requeueRef ! task
  }

  override def preStart(): Unit = {
    super.preStart()
    val tmpDirectoryPath = Paths.get(System.getProperty(SystemTempDirProperty)).resolve("digest")
    if (Files.notExists(tmpDirectoryPath)) {
      Files.createDirectory(tmpDirectoryPath)
    }
  }
}

object SupervisorActor {
  //TODO: configuration
  val MaxWorkersCount = 5

  def props(notificationsRef: ActorRef, requeueRef: ActorRef) = Props(new SupervisorActor(notificationsRef, requeueRef))

  case class Acknowledgment()
}
