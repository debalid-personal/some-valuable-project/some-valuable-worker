package com.staysanta.somevaluableworker

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem}
import akka.pattern.ask
import akka.stream.alpakka.amqp._
import akka.stream.alpakka.amqp.scaladsl.{AmqpSink, AmqpSource}
import akka.stream.scaladsl.{Sink, Source}
import akka.stream.{ActorMaterializer, OverflowStrategy}
import akka.util.{ByteString, Timeout}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.staysansata.somevaluableworker.api.{TaskNotification, TaskRequest, TaskRunRequest}
import com.staysanta.somevaluableworker.actors.SupervisorActor

object WorkerApp extends App {

  import scala.concurrent.duration._

  implicit lazy val system = ActorSystem("system")
  implicit lazy val materializer = ActorMaterializer()

  implicit val mapper = new ObjectMapper()
  mapper.registerModule(DefaultScalaModule)
  mapper.registerModule(new JavaTimeModule())

  //TODO: configuration
  val inputExchangeName = "task-requests"
  val outputExchangeName = "task-notifications"
  val connectionSettings = AmqpConnectionDetails("rabbit", 5672).withCredentials(AmqpCredentials("rabbit", "rabbit"))
  val requeueDelay = 10.seconds

  /**
    * All notifications about task progress are sent to "task-notifications" RabbitMQ exchange
    */
  val notifications = AmqpSink.simple(
    AmqpSinkSettings(connectionSettings).withExchange(outputExchangeName).withRoutingKey(outputExchangeName)
  ).contramap[String](ByteString(_)).contramap[TaskNotification](mapper.writeValueAsString(_))
  lazy val notificationsActorRef = Source.actorRef(5, OverflowStrategy.dropTail).log(outputExchangeName).to(notifications).run()

  /**
    * We need a possibility to requeue some task requests (e.g. if resources are not available), this is done by using this stream which is treated as an actor
    */
  val requeue = AmqpSink.simple(
    AmqpSinkSettings(connectionSettings).withExchange(inputExchangeName).withRoutingKey(inputExchangeName)
  ).contramap[String](ByteString(_)).contramap[TaskRunRequest](mapper.writeValueAsString(_))
  lazy val requeueActorRef = Source.actorRef(5, OverflowStrategy.dropTail).delay(requeueDelay).log(inputExchangeName).to(requeue).run()

  /**
    * This actor is responsible to answer incoming requests and managing underlying workers
    */
  val workersSupervisor: ActorRef = system.actorOf(SupervisorActor.props(notificationsActorRef, requeueActorRef), "supervisor")

  /**
    * This stream is a source of requests for task executions and tasks cancellation
    */
  val requests = AmqpSource(
    TemporaryQueueSourceSettings(connectionSettings, inputExchangeName).withRoutingKey(inputExchangeName),
    bufferSize = 10
  )
  implicit val timeout = Timeout(10, TimeUnit.MINUTES)
  requests.map(_.bytes.utf8String)
    .log(inputExchangeName)
    .map(mapper.readValue(_, classOf[TaskRequest]))
    .log(inputExchangeName)
    .mapAsync(1)(workersSupervisor ? _)
    .log(inputExchangeName)
    .runWith(Sink.ignore)
}
